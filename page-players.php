<?php
get_header();

//WP_Query

$args = array(
    'post_type' => 'bstore_player',
    'post_status' => 'publish',
    'posts_per_page' => 50,
    'orderby' => 'title',
    'order' => 'ASC',
);

$loop = new WP_Query($args);

if ($loop->have_posts()) :
    while (have_posts()) : $loop->the_post();
        get_template_part('template-parts/player-list-item');
    endwhile;
else : ?>
    <p>Sorry, no posts</p>
<?php endif;

get_footer();
