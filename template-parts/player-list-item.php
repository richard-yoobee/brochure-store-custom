<div>
    <h2>
        <a href="<?php site_url(); ?>/brochure-store/bstore_player/<?php echo $post->post_name; ?>">
            <?php
            if (has_post_thumbnail()) : ?>
                <img src="<?php the_post_thumbnail_url('thumbnail'); ?>" alt="=" <?php the_title(); ?>">
            <?php endif;
            the_title();
            ?>
        </a>
    </h2>
</div>