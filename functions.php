<?php

if(!function_exists('bstore_setup')){
    function bstore_setup(){
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');
        add_theme_support('custom-logo');
    }
}

add_action('after_setup_theme','bstore_setup');

/**
 * Register and Enqueue Styles.
 */
function bstore_register_styles() {
	$theme_version = wp_get_theme()->get( 'Version' );
	wp_enqueue_style( 'bstore-style', get_stylesheet_uri(), array(), $theme_version );
}

add_action( 'wp_enqueue_scripts', 'bstore_register_styles' );

/**
 * Register and Enqueue Scripts.
 */
function bstore_register_scripts() {
	wp_enqueue_script('bstore_responsive_nav', get_template_directory_uri() . '/assets/js/responsive-nav.js');
}

add_action( 'wp_enqueue_scripts', 'bstore_register_scripts' );

// require_once get_template_directory() . '/inc/custom-post-types.php';

require_once get_template_directory() . '/inc/custom-taxonomies.php';

require_once get_template_directory() . '/inc/customization.php';

require_once get_template_directory() . '/inc/woo-commerce.php';