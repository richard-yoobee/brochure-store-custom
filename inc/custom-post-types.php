<?php
function bstore_create_post_types(){

    //player
    register_post_type('bstore_player', 
        array('labels' => array(
            'name' => __('Players'),
            'singular_name' => __('Player')
        ),
        'public' => true,
        'has_archive' => false,
        'menu_position' => 5,
        'show_in_rest' => true,
        'supports' => array('title', 'editor', 'thumbnail')
        )
);
}

add_action('init', 'bstore_create_post_types');