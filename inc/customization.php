<?php

function bstore_customize_register($wp_customize)
{
    $wp_customize->get_setting('blogname')->transport         = 'postMessage';
    $wp_customize->get_setting('blogdescription')->transport  = 'postMessage';
    $wp_customize->get_setting('header_textcolor')->transport = 'postMessage';

    //about text 

    $wp_customize->add_section('bstore_about_section', array(
        'title' => __('About'),
        'priority' => 10
    ));

    $wp_customize->add_setting('bstore_about_text_setting', array(
        'cabability' => 'edit_theme_options'
    ));

    $wp_customize->add_control('bstore_about_text_control', array(
        'type' => 'textarea',
        'section' => 'bstore_about_section',
        'label' => __('About Us Text'),
        'settings' => 'bstore_about_text_setting'
    ));

    // about image

    $wp_customize->add_setting('bstore_about_image_setting', array(
        'cabability' => 'edit_theme_options'
    ));

    $wp_customize->add_control(new WP_Customize_Image_Control(
        $wp_customize, 
        'bstore_about_image_control', 
        array(
        'section' => 'bstore_about_section',
        'label' => __('About Us Image'),
        'settings' => 'bstore_about_image_setting'
    )));
}
add_action('customize_register', 'bstore_customize_register');
