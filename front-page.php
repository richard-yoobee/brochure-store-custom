<?php
get_header(); ?>
<section>
    Landing
</section>
<section>
    Our players
    teaser stuff
<?php 
$args = array(
    'post_type' => 'bstore_player',
    'post_status' => 'publish',
    'posts_per_page' => 3,
    'orderby' => 'title',
    'order' => 'ASC',
);

$loop = new WP_Query($args);

if ($loop->have_posts()) :
    while (have_posts()) : $loop->the_post();
        get_template_part('template-parts/player-list-item');
    endwhile;
else : ?>
    <p>Sorry, no posts</p>
<?php endif;
?>

    cta
</section>
<section>
    Something else
</section>

<?php get_footer();
